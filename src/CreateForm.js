import React from "react";
import { useHistory } from "react-router-dom";
import { useData } from "./DataContext";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers";
import { PrimaryButton } from "./components/PrimaryButton";
import { MainContainer } from "./components/MainContainer";
import { Form } from "./components/Form";
import { Input } from "./components/Input";
import * as yup from "yup";
import { parsePhoneNumberFromString } from "libphonenumber-js";
import RadioGroup from "./RadioGroup";
import { object, string } from "yup";
import { FileInput } from "./components/FileInput";
import { useDispatch, useSelector } from "react-redux";
import { currentName } from "./rootSlice";
import TemporaryModal from "./Modal.js";
import axios from "axios";
import Temp from "./Temp";

const schema = yup.object().shape({
    fullName: yup
        .string()
        .matches(/^([^0-9]*)$/, "First name should not contain numbers")
        .required("First name is a required field"),
    email: yup
        .string()
        .email("Email should have correct format")
        .required("Email is a required field"),
    gender: string().required(),
});

const normalizePhoneNumber = (value) => {
    const phoneNumber = parsePhoneNumberFromString(value);
    if (!phoneNumber) {
        return value;
    }

    return phoneNumber.formatInternational();
};

const radioOptions = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Others", value: "others" },
];

const CreateForm = () => {
    const [modalShow, setModalShow] = React.useState(false);
    const { setValues, data } = useData();

    const dispatch = useDispatch();
    const history = useHistory();
    const name = useSelector((state) => state.fullName);

    const { control, register, handleSubmit, errors, getValues } = useForm({
        defaultValues: {
            fullName: data.fullName,
            email: data.email,
            phoneNumber: data.phoneNumber,
            gender: data.gender,
            files: data.files,
        },
        mode: "onBlur",
        resolver: yupResolver(schema),
    });

    const onSubmit = (data) => {
        history.push("./ViewForm");
        setValues(data);
        const values = getValues();
        dispatch(currentName(values.fullName));
        const name = values.fullName;
        const gender = values.gender;
        const email = values.email;
        const mobile = values.phoneNumber;
        const dataValues = {
            name,
            gender,
            email,
            mobile,
        };
        console.log(dataValues);
        const config = {
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": "true",
            },
        };
        const sendPostRequest = async () => {
            try {
                const resp = await axios.post(
                    "http://localhost:4000/user/add",
                    {
                        name: "Prakshit",
                        gender: "Male",
                        email: "Rakshit@gmail.com",
                        mobile: 8989454576,
                        category: "..",
                        profile_pic: "..",
                    },
                    config
                );
                console.log(resp.data);
            } catch (err) {
                // Handle Error Here
                console.error(err);
            }
        };

        sendPostRequest();
    };

    return (
        <MainContainer>
            <Form onSubmit={handleSubmit(onSubmit)}>
                <Input
                    ref={register}
                    id="fullName"
                    type="text"
                    label="Full Name"
                    name="fullName"
                    error={!!errors.fullName}
                    helperText={errors?.fullName?.message}
                    required
                />
                <Input
                    ref={register}
                    id="email"
                    name="email"
                    type="email"
                    placeholder="email"
                    label="email"
                    error={!!errors.email}
                    helperText={errors?.email?.message}
                    required
                />
                <Input
                    ref={register}
                    id="phoneNumber"
                    type="tel"
                    label="Phone Number"
                    name="phoneNumber"
                    placeholder="phoneNumber"
                    onChange={(event) => {
                        event.target.value = normalizePhoneNumber(
                            event.target.value
                        );
                    }}
                />
                <RadioGroup
                    label="Gender"
                    name="gender"
                    options={radioOptions}
                    ref={register}
                />
                <FileInput name="files" control={control} />
                <PrimaryButton onClick={onSubmit}>Next</PrimaryButton>
                <TemporaryModal
                    show={modalShow}
                    onClick={() => {
                        const values = getValues();
                    }}
                    onHide={() => setModalShow(false)}
                />
            </Form>
        </MainContainer>
    );
};

export default CreateForm;
