import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import CreateForm from "./CreateForm";
import ViewForm from "./ViewForm";
import Temp from "./Temp";

function App() {
    return (
        <Router>
            <div>
                <nav>
                    <ul>
                        <li>
                            <Link to="/users/create">Home</Link>
                        </li>
                        <li>
                            <Link to="/users/create">Create</Link>
                        </li>
                        <li>
                            <Link to="/users/view">View</Link>
                        </li>
                    </ul>
                </nav>
                <Switch>
                    <Route path="/">{<ViewForm />}</Route>
                    <Route path="/users/create">{<CreateForm />}</Route>
                    <Route path="/users/view">{<ViewForm />}</Route>
                </Switch>
            </div>
        </Router>
    );
}

export default App;
