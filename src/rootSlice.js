import { createSlice } from "@reduxjs/toolkit";

const rootSlice = createSlice({
    name: "root",
    initialState: {
        fullName: "",
        email: "example@mail.com",
        phone: "91",
        gender: "male",
    },
    reducers: {
        currentName: (state, action) => {
            state.fullName = action.payload;
        },
        currentEmail: (state, action) => {
            state.email = action.payload;
        },
        currentPhone: (state, action) => {
            state.phone = action.payload;
        },
        currentGender: (state, action) => {
            state.gender = action.payload;
        },
    },
});

export const reducer = rootSlice.reducer;

export const {
    currentName,
    currentEmail,
    currentPhone,
    currentGender,
} = rootSlice.actions;
